#Chargement des packages ----
library(tidyverse)

#Chargement des données des lieux de tournages à Paris ----
df_tournages <- read_rds("data/lieux-de-tournage-a-paris_enrichi.rds")

# 1ère exploration  ----
# Les tournages selon les années
df_tournages_annee <- df_tournages %>%
  group_by(annee_du_tournage) %>%
  summarise(NbTournages = n())





# Exercice 1 : Explorer les notes des films avec différents types de graphiques :)  ----


#Vous disposez du tableau principal df_tournages et des tableaux suivants : 

#Tableau1
df_note_annee <- df_tournages %>% 
  group_by(annee_du_tournage) %>%
  summarise( note_moy = mean(vote_average, na.rm = TRUE)) 

#Tableau2
df_note_genre  <- df_tournages %>%
  separate_rows(genres, sep = ";") %>%
  filter(genres %in% c("Drame", "Comédie", "Romance", "Thriller", "Action", "Horreur")) %>%
  group_by(genres, annee_du_tournage) %>%
  summarise( note_moy = mean(vote_average)) 

#Tableau3
df_note_genre_cp  <- df_tournages %>%
  separate_rows(genres, sep = ";") %>%
  filter(genres %in% c("Drame", "Comédie", "Romance", "Thriller", "Action", "Horreur")) %>%
  group_by(genres, code_postal) %>%
  summarise( note_moy = mean(vote_average)) 





# Exercice 2 : Explorer les langues originales des films avec différents types de graphiques :)  ----
#Vous disposez du tableau principal df_tournages et des tableaux suivants : 

#Tableau 1
df_lg <- df_tournages %>%
  filter(!is.na(original_language)) %>% 
  group_by(original_language) %>%
  summarise( NbTournages = n()) %>%
  mutate(pct_tournage = (NbTournages/sum(NbTournages))*100)

#Tableau 2
df_lg_note <- df_tournages %>%
  filter(!is.na(original_language)) %>%
  group_by(original_language) %>%
  summarise( note_moy = mean(vote_average)) 

  
  

# Exercice 3 : Explorer le genre des films avec différents types de graphiques :)  ----

#Vous disposez du tableau principal df_tournages et des tableaux suivants : 

#Tableau 1
df_genre <- df_tournages %>%
  separate_rows(genres, sep = ";") %>%
  filter(genres %in% c("Drame", "Comédie", "Romance", "Thriller", "Action", "Horreur")) %>%
  group_by(genres) %>% summarise(NbTournages = n())
  

#Tableau 2
df_genre_annee <- df_tournages %>%
  separate_rows(genres, sep = ";") %>%
  filter(genres %in% c("Drame", "Comédie", "Romance", "Thriller", "Action", "Horreur")) %>%
  group_by(genres, annee_du_tournage) %>% summarise(NbTournages = n()) 


#Tableau 3
df_genre_point <- df_tournages %>%
  separate_rows(genres, sep = ";") %>%
  filter(genres %in% c("Drame", "Comédie", "Romance", "Thriller", "Action", "Horreur"))







